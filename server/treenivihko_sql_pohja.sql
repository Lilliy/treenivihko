-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.10-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             12.0.0.6468
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for treenivihko
CREATE DATABASE IF NOT EXISTS `treenivihko` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `treenivihko`;

-- Dumping structure for table treenivihko.breeds
CREATE TABLE IF NOT EXISTS `breeds` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `item_order` int(11) unsigned DEFAULT NULL COMMENT 'Numerical order that can be used to sort this list',
  PRIMARY KEY (`id`),
  KEY `name` (`name`(250))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='List of dog breeds available for selection.';

-- Dumping data for table treenivihko.breeds: 0 rows
/*!40000 ALTER TABLE `breeds` DISABLE KEYS */;
/*!40000 ALTER TABLE `breeds` ENABLE KEYS */;

-- Dumping structure for table treenivihko.dogs
CREATE TABLE IF NOT EXISTS `dogs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL COMMENT 'Link to user who owns this dog',
  `breed_id` int(11) unsigned DEFAULT NULL COMMENT 'Link to dog''s breed table',
  `sport_id_last_done` int(11) unsigned DEFAULT NULL COMMENT 'Link to sports. Quick access to last sport that was done.',
  `name` varchar(255) NOT NULL COMMENT 'Dog''s calling name',
  `official_name` varchar(255) DEFAULT NULL COMMENT 'Dog''s official name',
  `image_filename` varchar(255) DEFAULT NULL COMMENT 'Filename of this dog''s image file',
  `dob` date DEFAULT NULL,
  `created` date DEFAULT NULL,
  `last_modified` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Table for dogs.';

-- Dumping data for table treenivihko.dogs: 0 rows
/*!40000 ALTER TABLE `dogs` DISABLE KEYS */;
/*!40000 ALTER TABLE `dogs` ENABLE KEYS */;

-- Dumping structure for table treenivihko.excercises
CREATE TABLE IF NOT EXISTS `excercises` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_excercise_id` int(11) unsigned DEFAULT NULL COMMENT 'Link to parent excercise for this excercise',
  `user_id` int(11) unsigned DEFAULT NULL COMMENT 'Link to user who added this excercise and can view it',
  `public` tinyint(1) unsigned DEFAULT NULL COMMENT 'Boolean for excercise''s visibility for all users',
  `selectable` tinyint(1) unsigned DEFAULT NULL COMMENT 'Boolean if excercise is selectable or simply a container for other excercises',
  `name` varchar(255) NOT NULL,
  `created` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `parent_excercise_id` (`parent_excercise_id`),
  KEY `public` (`public`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Excercises dogs can perform.';

-- Dumping data for table treenivihko.excercises: 0 rows
/*!40000 ALTER TABLE `excercises` DISABLE KEYS */;
/*!40000 ALTER TABLE `excercises` ENABLE KEYS */;

-- Dumping structure for table treenivihko.sports
CREATE TABLE IF NOT EXISTS `sports` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `name_initials` char(2) DEFAULT NULL COMMENT 'Two character short version of name',
  `color` char(7) DEFAULT NULL COMMENT 'Sports'' associated color in hex (#ffffff)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='List of dog sports.';

-- Dumping data for table treenivihko.sports: 0 rows
/*!40000 ALTER TABLE `sports` DISABLE KEYS */;
/*!40000 ALTER TABLE `sports` ENABLE KEYS */;

-- Dumping structure for table treenivihko.training_reports
CREATE TABLE IF NOT EXISTS `training_reports` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL COMMENT 'Link to which user created this training session',
  `sport_id` int(11) unsigned DEFAULT NULL COMMENT 'Link to what sport this training session relates to',
  `text_what_done` text DEFAULT NULL,
  `text_how_went` text DEFAULT NULL,
  `text_what_next` text DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `sport_id` (`sport_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Coverage of a training session.';

-- Dumping data for table treenivihko.training_reports: 0 rows
/*!40000 ALTER TABLE `training_reports` DISABLE KEYS */;
/*!40000 ALTER TABLE `training_reports` ENABLE KEYS */;

-- Dumping structure for table treenivihko.training_report_excercises
CREATE TABLE IF NOT EXISTS `training_report_excercises` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `training_report_id` int(11) unsigned NOT NULL,
  `excercise_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `excercise_id` (`excercise_id`),
  KEY `dog_id` (`training_report_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Table that links multiple excercises for a training report.';

-- Dumping data for table treenivihko.training_report_excercises: 0 rows
/*!40000 ALTER TABLE `training_report_excercises` DISABLE KEYS */;
/*!40000 ALTER TABLE `training_report_excercises` ENABLE KEYS */;

-- Dumping structure for table treenivihko.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_group_id` int(11) unsigned NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `street_address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL COMMENT 'Timestamp when user was created',
  `last_modified` datetime DEFAULT NULL COMMENT 'Timestamp when user was last modified',
  `last_login` datetime DEFAULT NULL COMMENT 'Timestamp when user was last logged in',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING HASH,
  KEY `email` (`email`(250)),
  KEY `password` (`password`(250))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='App users who can log in and have multiple dogs.';

-- Dumping data for table treenivihko.users: 0 rows
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table treenivihko.user_groups
CREATE TABLE IF NOT EXISTS `user_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_group_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Groups for different user types, eg. super user, elevated user, normal user, etc.';

-- Dumping data for table treenivihko.user_groups: 0 rows
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
