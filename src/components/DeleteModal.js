import React from 'react';
import { Modal, Box, Typography, Stack, Button } from '@mui/material'

// Styles for the component. Came with the package.
const style = {
	position: 'absolute',
	top: '50%',
	left: '50%',
	transform: 'translate(-50%, -50%)',
	width: 400,
	bgcolor: 'background.paper',
	border: '2px solid #000',
	boxShadow: 24,
	p: 4,
};
// Takes in props from DogCard or TrainingCard so the modal knows what is being deleted (target).
// props:
// open & setClose = useState from the parent component. To see if the modal is open or closed.
// target = string from the component where this component was called from and shows the proper text in the opened modal. 
// deleteObject = function that is called from the parent component to delete the object in question. 
const DeleteModal = ({ open, setClose, target, deleteObject }) => {
	return (
		<Modal
			open={open}
			onClose={setClose}
		>
			<Box sx={style}>
				<Stack spacing={"20px"}>
					<Typography variant="h6" component="h2">
						Haluatko varmasti poistaa {target}?
					</Typography>
					<Stack direction="row" spacing={"10px"}>
						<Button
							variant="contained"
							color="error"
							onClick={deleteObject}>
							Poista</Button>
						<Button
							variant="outlined"
							color="error"
							onClick={setClose}>
							Peruuta
						</Button>
					</Stack>
				</Stack>
			</Box>
		</Modal>
	);
};

export default DeleteModal;
