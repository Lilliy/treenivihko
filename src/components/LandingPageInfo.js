import React from 'react';
import { Box, Typography } from '@mui/material';

// For now, there is only mock text. Later there will be info on new updates and bugfixes. Perhaps some info on the site itself and its uses.

const LandingPageInfo = () => {
	return (
		<Box sx={{ py: { xs: '15px', sm: '30px', md: '50px' }, px: "20px" }}>

			<Typography> Koirien treenien kirjaamiseen soveltuva applikaatio.</Typography>
			<Typography> Viimeisimmät päivitykset:</Typography>
			<Typography>Rekisteröityminen on mahdollista. Rekisteröitymistunnus tulee pyytää Lilli Luomakoskelta. </Typography>
		</Box>
	);
};

export default LandingPageInfo;
