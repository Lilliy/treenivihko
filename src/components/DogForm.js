import React, { useEffect, useState, useContext } from 'react';
import { Button, Grid, Select, TextField, Box, MenuItem, Typography } from '@mui/material';
import PhotoCamera from '@mui/icons-material/PhotoCamera';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { MobileDatePicker } from '@mui/x-date-pickers/MobileDatePicker';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import AuthContext from '../context/UserContext';
import beginningAddress from './Components';
import { useNavigate } from 'react-router-dom';

// Get all breeds from backend. Request GET. Returns an object that contains requested "data" on success and if it fails the "data" value is empty and status "error".
// Required fields: source ("breeds" / "sports")
// Optional fields: -
const getBreedData = async () => {
	return await axios.get(`${beginningAddress}get_data.php?source=breeds`);
};

const DogForm = ({ edit }) => {

	const navigate = useNavigate();

	const { user } = useContext(AuthContext);
	// Empty states where to either load old data for editing or to set new data for new object.

	// TODO: "Nämä voisi yhdistää yhdeksi tilaksi, mikä on objekti"
	// const [dogInfo, setDogInfo] = useState({
	// 	officialName,
	// 	name,
	// 	date,
	// 	breed,
	// 	sex
	// });

	const currentDate = new Date();
	const [officialName, setOfficialName] = useState('');
	const [name, setName] = useState('');
	const [date, setDate] = useState(currentDate);
	const [breed, setBreed] = useState('');
	const [sex, setSex] = useState('');

	// state which is initialized as an empty array and filled with breeds data when rendered for the first time.
	const [breeds, setBreeds] = useState([]);

	// useEffect takes an array as an dependency list and  when the variables in the array change the function is called. If the array is empty it is run at every re-rendering. So for this useEffect it will call the getBreedData at every re-rendering and sets the breeds in to the wanted state.
	useEffect(() => {
		getBreedData()
			.then((response) => {
				setBreeds(response.data.data);
			})
			.catch((error) => {
				console.log(error, "rotujen haun error");
			})
	}, []);

	// gets the id from address
	const { id } = useParams();

	// When "edit" changes it checks if edit=true and will get the dog's data and then set the values gotten as a response.
	// note to self:  useEffect has an array (edit & id) as a dependency list and  when the variables in the array change the function is called. So for this, when the edit or id changes it will check if edit=true and get the data wanted and set it to the proper states. If the array changes (the user changes the dogs id or removes it) it re-renders the form and gets the new data or empties the fields if there is no id given
	// AXIOS Gets users single dog's data from backend. Returns all of user's dogs or a single dog, if dog ID is specified. Returns an object that contains requested "data" on success and if it fails the "data" value is empty and status "error". Requires authentication
	// Required fields: -
	// Optional fields: id (dog id)

	// Bäkkäri muuttuu kohta, eikä user id:tä tarvita enää kohta. Tokeni välittyy backendille myöhemmin. Poistan tämän kommentin kun muutos tuotu.
	useEffect(() => {
		const getDogData = async () => {
			const token = user.token
			if (token) {
				const headers = {
					authorization: `Bearer ${token}`,
				};
				const response = await axios
					.get(`${beginningAddress}get_user_dogs.php?&id=${id}`, { headers })
					.catch((event) => {
						alert(event.response.data.error_msg)
					})

				return response;
			}

			return null;
		};
		if (edit) {
			getDogData()
				.then((response) => {
					setOfficialName(response.data.data.official_name)
					setName(response.data.data.name)
					setDate(response.data.data.dob)
					setBreed(response.data.data.breed_id)
					setSex(response.data.data.sex)
				})
				.catch((error) => {
					console.log(error, "Koiran datan haun error");
				})
		}

	}, [edit, id, user]);

	// this function is called when user submits the form. The dogObject has been set before and the names have been given for the backend to understand them. Backend uses _ when frontend uses camelcase. If edit=true it posts modify_dog where the old data is replaced with new. If edit=false (else) it will create a new dog.

	const submitDog = (event) => {
		event.preventDefault()
		const dogObject = {
			official_name: officialName,
			name: name,
			dob: date,
			breed_id: breed,
			sex: sex
		}
		// AXIOS if edit. Returns an object that contains "status=ok" on success and if it fails "status=error". Updates existing dog into database. Empty or missing fields are turned into nulls.
		// Required fields: id, breed_id, name
		// Optional fields: official_name, dob, sex
		// Requires authentication.
		if (edit) {
			const modifyDogData = async (dogObject) => {
				const token = user.token
				if (token) {
					const headers = {
						authorization: `Bearer ${token}`,
					};
					const response = await axios
						.patch(`${beginningAddress}modify_dog.php`, dogObject, { headers })
						.catch((event) => {
							alert(event.response.data.error_msg)
						})

					return response;
				}

				return null;
			};
			dogObject.id = id;
			modifyDogData(dogObject)
				.then(response => {
					navigate('/alldogs')
				})
				.catch((error) => {
					console.log(error, "koiran muokkauksen error");
				});
		}
		// AXIOS if creating new dog. Returns an object that contains "status=ok" on success and if it fails "status=error". Creates a new dog into database. Empty or missing fields are turned into nulls.
		// Required fields: name
		// Optional fields: official_name, dob, breed_id, sex
		// Requires authentication.
		else {
			const saveNewDogData = async (dogObject) => {
				const token = user.token
				if (token) {
					const headers = {
						authorization: `Bearer ${token}`,
					};
					const response = await axios
						.put(`${beginningAddress}save_dog.php`, dogObject, { headers })
						.catch((event) => {
							alert(event.response.data.error_msg)
						})

					return response;
				}

				return null;
			};
			saveNewDogData(dogObject)
				.then(response => {
					navigate('/alldogs')
				})
				.catch((error) => {
					console.log(error, "Koiran lisäyksen error");
				});
		}
	}
	function subtractYears(numOfYears, date = new Date()) {
		date.setFullYear(date.getFullYear() - numOfYears);

		return date;
	}

	return (
		<LocalizationProvider dateAdapter={AdapterDateFns}>
			<Box sx={{
				width: "90%",
				my: 0,
				mx: "auto",
				px: "20px",
				py: "20px"
			}}>
				<Typography
					fontSize="30px"
					component="h2"
					pb="20px">
					Luo uusi koira
				</Typography>
				<form onSubmit={submitDog}>
					<Grid container spacing={3}>
						<Grid item xs={12} sm={6}>
							<TextField
								sx={{
									width: "100%"
								}}
								id="outlined-basic"
								label="Virallinen nimi"
								variant="outlined"
								name="officialName"
								value={officialName}
								onChange={(event) => setOfficialName(event.target.value)} />
						</Grid>
						<Grid item xs={12} sm={6}>
							<TextField
								sx={{
									width: "100%"
								}}
								id="outlined-basic"
								label="Kutsumanimi"
								variant="outlined"
								name="name"
								value={name}
								onChange={(event) => setName(event.target.value)} />
						</Grid>
						<Grid item xs={12} sm={6}>
							<Select
								sx={{
									width: "100%"
								}}
								id="breed_id"
								value={breed}
								onChange={(event) => setBreed(event.target.value)}
								displayEmpty
								inputProps={{ 'aria-label': 'Valitse laji' }}
							>
								<MenuItem value="" defaultValue>
									<em>Valitse rotu</em>
								</MenuItem>
								{breeds.map(breed =>
									<MenuItem key={breed.id} value={breed.id}>{breed.name}</MenuItem>)
								}
							</Select>
						</Grid>
						<Grid item xs={12} sm={6}>
							<MobileDatePicker

								label="Syntymäpäivä"
								inputFormat="dd/MM/yyyy"
								value={date}
								minDate={subtractYears(15)}
								maxDate={currentDate}
								onChange={(newValue) =>
									setDate(`${newValue.getFullYear()}-${('0' + (newValue.getMonth() + 1)).slice(-2)}-${('0' + (newValue.getDate())).slice(-2)}`)}
								renderInput={(params) => <TextField sx={{
									width: "100%"
								}} {...params} />}
							/>
						</Grid>
						<Grid item xs={12} sm={6}>
							<Select
								sx={{
									width: "100%"
								}}
								id="sex"
								value={sex}
								onChange={(event) => setSex(event.target.value)}
								displayEmpty
								inputProps={{ 'aria-label': 'Valitse koira' }}
							>
								<MenuItem value="" defaultValue>
									<em>Valitse sukupuoli</em>
								</MenuItem>
								<MenuItem value="2">Narttu</MenuItem>
								<MenuItem value="1">Uros</MenuItem>

							</Select>
						</Grid>
						<Grid item xs={12} sm={6}>
							<Button variant="contained" component="label" endIcon={<PhotoCamera />}>
								Lisää kuva
								<input hidden accept="image/*" multiple type="file" />
							</Button>
						</Grid>
						<Grid item xs={12} sm={12}>
							<Button
								type="submit"
								variant="contained"
							>Tallenna</Button>
						</Grid>
					</Grid>
					{/* <Stack spacing={"20px"} sx={{ p: "20px" }}>
					<div>
						<label htmlFor="officialName">Virallinen nimi</label><br />
						<input
							type="text"
							name="officialName"
							value={officialName}
							onChange={(event) => setOfficialName(event.target.value)}
						/>
					</div>
					<div>
						<label htmlFor="name">Kutsumanimi</label><br />
						<input
							type="text"
							name="name"
							value={name}
							onChange={(event) => setName(event.target.value)}
						/>
					</div>
					<div>
						<label htmlFor="breed_id">Valitse rotu</label>
						<br />
						<select
							id="breed_id"
							name="breed_id"
							value={breed}
							onChange={(event) => setBreed(event.target.value)}
						>
							<option value="" defaultValue>Select your option</option>
							{breeds.map(breed =>
								<option key={breed.id} value={breed.id}>{breed.name}</option>)
							}
						</select>
					</div>
					<div>
						<label htmlFor="dob">Syntynyt:</label><br />
						<input
							type="date"
							id="dob"
							name="dob"
							max="2023-12-31"
							value={date}
							onChange={(event) => setDate(event.target.value)} />
					</div>
					<div>
						<label htmlFor="sex">Sukupuoli</label>
						<br />
						<select
							id="sex"
							name="sex"
							value={sex}
							onChange={(event) => setSex(event.target.value)}>
							<option value="0" defaultValue>Select your option</option>
							<option value="2">Narttu</option>
							<option value="1">Uros</option>
						</select>
					</div>
					<button type="submit">Tallenna</button>
				</Stack> */}
				</form>
			</Box>
		</LocalizationProvider >
	);
};

export default DogForm;
