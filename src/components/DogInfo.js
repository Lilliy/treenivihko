import { Box, Paper, Stack, Typography } from '@mui/material';
import React from 'react';

// This component is called in DogPage and shows a dog's basic info.

// This function takes in a number value from backend and gives a string in return. This makes it easier for backend to only save one number rather than a string. 1=male & 2=female and it can also be empty.
// TODO: kirjoita funktio nätimmin? Jotenkin, että siinä ei tarvitsisi let:iä ?
function defineGender(gender) {
	let result;
	if (gender === 2) {
		result = "Narttu";
	}
	else if (gender === 1) {
		result = "Uros";
	}
	else {
		result = "Ei määritetty";
	}
	return result;

}
// prop dog is passed from the parent component DogPage where the data from backend is set into useState.

const DogInfo = ({ dog }) => {

	return (
		<Paper sx={{ padding: "20px", width: { xs: "90%", sm: "500px", md: "600px" } }}>
			<Box>
				<Stack
					direction={{ xs: "column", sm: "row" }}
					alignItems="center">
					<Box
						component="img"
						sx={{
							height: "45vh",
							maxHeight: "500px",
							alignItems: "center",
							pr: { xs: '0px', sm: '10px' }
						}}
						alt="Kuva koirasta"
						src={dog.image_filename} />
					<Box>
						<Typography variant="h3">{dog.name}</Typography>
						<Stack direction="row" spacing={"20px"}>
							<Typography sx={{ fontWeight: 'bold' }}>Virallinen nimi:</Typography>
							<Typography>{dog.official_name}</Typography>
						</Stack>
						<Stack direction="row" spacing={"20px"}>
							<Typography sx={{ fontWeight: 'bold' }}>Sukupuoli:</Typography>
							<Typography> {defineGender(parseInt(dog.sex))}
							</Typography>
						</Stack>
						<Stack direction="row" spacing={"20px"}>
							<Typography sx={{ fontWeight: 'bold' }}>Rotu:</Typography>
							<Typography>{dog.breed_name}</Typography>
						</Stack>
						<Stack direction="row" spacing={"20px"}>
							<Typography sx={{ fontWeight: 'bold' }}>Syntymäpäivä:</Typography>
							<Typography>{dog.dob}</Typography>
						</Stack>
					</Box>
				</Stack>
			</Box>
		</Paper>
	);
};

export default DogInfo;

// jos tietokannasta tulee numero joka vastaa sukupuolta, mikä on paras tapa muuttaa se tekstiksi? :? on ternary
// Mikä fiksuin tapa tuoda päivämäärät? Backend syöttää oikeassa muodossa vai muokata frontissa? frontti formatoi.