import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Container, Box, Avatar, Typography, TextField, Button, Tooltip } from '@mui/material';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import axios from 'axios';
import beginningAddress from './Components';

const register = async (username, email, password, password_again, register_password) => {
	return await axios.put(`${beginningAddress}register_user.php`, {
		username, email, password, password_again, register_password
	});
};
// From below when handleSubmits the proper data is sent here for it to be sent to the backend

// TODO: "Nämä voisi yhdistää yhdeksi tilaksi, mikä on objekti" Malli otettu DogFormista.
// const [dogInfo, setDogInfo] = useState({
// 	officialName,
// 	name,
// 	date,
// 	breed,
// 	sex
// });

const RegisterForm = () => {
	const [username, setUsername] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [password_again, setPasswordAgain] = useState('');
	const [register_password, setRegister_password] = useState('');
	// useState takes a value to preserve it. In the form below the fields take in the value and then it is set to the variable.

	const navigate = useNavigate();

	const validateOnSubmit = () => {
		if (username && email && password && password_again && register_password) {
			if (password === password_again) {
				return true;
			} else {
				alert('Salasanat eivät täsmää.');
				return false;
			}
		} else {
			alert('Täytä kaikki kentät.');
			return false;
		}
	};
	// this is to check if all fields are filled in and it checks if the password and password_again is the same or not.

	const handleSubmit = (event) => {
		event.preventDefault();
		if (validateOnSubmit()) {
			register(username, email, password, password_again, register_password)
				.then((response) => {
					alert('Rekisteröityminen onnistui! Kirjaudu sisään.')
					navigate('/login')
				})
				.catch((event) => {
					alert(event.response.data.error_msg);

				});
		}
	}
	// when handleSubmit is invoked it checks the validation from above and if it is returned true the data go to the register function where it sends the data to the backend and it alerts the user of success and navigates to front page for user to sign in. For now there is no email verification. If the validation returns false it will alert the user with the proper response.
	return (
		<Container component="main" maxWidth="xs">
			<Box
				sx={{
					marginTop: "20px",
					display: 'flex',
					flexDirection: 'column',
					alignItems: 'center',
				}}
			>
				<Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
					<LockOutlinedIcon />
				</Avatar>
				<Typography component="h1" variant="h5">
					Luo tunnus
				</Typography>
				<Box component="form"
					onSubmit={handleSubmit}
					noValidate sx={{ mt: 1 }}>
					<TextField
						margin="normal"
						required
						fullWidth
						id="userName"
						label="Käyttäjätunnus"
						name="userName"
						value={username}
						onChange={({ target }) =>
							setUsername(target.value)}
						// autoComplete="userName"
						autoFocus
					/>
					<TextField
						margin="normal"
						required
						fullWidth
						id="email"
						label="Sähköpostiosoite"
						name="email"
						value={email}
						onChange={({ target }) => setEmail(target.value)}
					// autoComplete="email"
					/>
					<TextField
						margin="normal"
						required
						fullWidth
						name="password"
						label="Salasana"
						type="password"
						id="password"
						value={password}
						onChange={({ target }) => setPassword(target.value)}
					/>
					<TextField
						margin="normal"
						required
						fullWidth
						name="password"
						label="Salasana uudelleen"
						type="password"
						id="password_again"
						value={password_again}
						onChange={({ target }) => setPasswordAgain(target.value)}
					/>
					<Tooltip title="Rekisteröityminen on rajoitettu. Pyydä tunnus Lilliltä.">
						<TextField
							margin="normal"
							required
							fullWidth
							name="register_password"
							label="Rekisteröitymistunnus"
							id="register_password"
							value={register_password}
							onChange={({ target }) => setRegister_password(target.value)}
						/>
					</Tooltip>
					<Button
						type="submit"
						fullWidth
						variant="contained"
						sx={{ mt: 3, mb: 2 }}
					>
						Luo tunnus
					</Button>
				</Box>
			</Box>
		</Container>
	);
};

export default RegisterForm;
