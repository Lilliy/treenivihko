import React, { useState, useContext } from 'react';
import { Container, Box, Avatar, Typography, TextField, FormControlLabel, Button, Checkbox } from '@mui/material';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import AuthContext from '../context/UserContext';


const LoginForm = () => {
	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');
	// useState takes a value to preserve it. In the form below the fields take in the value and then it is set to the variable.

	const { onLogIn, user } = useContext(AuthContext);
	console.log(user)
	const handleSubmit = (event) => {
		event.preventDefault();
		onLogIn(username, password);
		// on handleSubmit it calls the onLogIn where it passes the username and password to useContext where the users data is saved to local storage and the user is logged in.

	}
	return (
		<Container component="main" maxWidth="xs">
			<Box
				sx={{
					marginTop: '20px',
					display: 'flex',
					flexDirection: 'column',
					alignItems: 'center',
				}}
			>

				<Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
					<LockOutlinedIcon />
				</Avatar>
				<Typography component="h1" variant="h5">
					Kirjaudu sisään
				</Typography>
				<Box component="form"
					onSubmit={handleSubmit}
					noValidate sx={{ mt: 1 }}>
					<TextField
						margin="normal"
						required
						fullWidth
						id="userName"
						label="Käyttäjätunnus"
						name="userName"
						value={username}
						onChange={({ target }) => setUsername(target.value)}
						// autoComplete="userName"
						autoFocus
					/>
					<TextField
						margin="normal"
						required
						fullWidth
						name="password"
						label="Salasana"
						type="password"
						id="password"
						value={password}
						onChange={({ target }) => setPassword(target.value)}
					// autoComplete="current-password"
					/>
					<FormControlLabel
						control={<Checkbox value="remember" color="primary" />}
						label="Muista minut"
					/>
					<Button
						type="submit"
						fullWidth
						variant="contained"
						sx={{ mt: 3, mb: 2 }}
					>
						Kirjaudu sisään
					</Button>
					{/* <Grid container>
						<Grid item xs>
							<Link href="/" variant="body2">
								Forgot password?
							</Link>
						</Grid>
						<Grid item>
							<Link href="/" variant="body2">
								{"Don't have an account? Sign Up"}
							</Link>
						</Grid>
					</Grid> */}
				</Box>
			</Box>
		</Container>
	);
};

export default LoginForm;
