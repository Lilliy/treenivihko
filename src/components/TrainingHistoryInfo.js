import React from 'react';
import TrainingCard from './TrainingCard';
import { Stack } from '@mui/material';

// This component is called in DogPage to show the dog's trainings. Returns singular card components.

// props passed down from DogPage.
// trainings: data gotten from backend and set into useState. Passed down to card component.
// refresh: boolean, used to reload the page after a training card is deleted.
const TrainingHistoryInfo = ({ trainings, refresh }) => {
	const hasTrainings = trainings?.length > 0;
	const trainingHistories = hasTrainings && trainings
		// .sort( @todo järjestä päivämäärän mukaan )
		.map(training => {
			return (
				<TrainingCard key={training.id} training={training} refresh={refresh} />
			)
		})

	return (
		<Stack spacing={"20px"} justifyContent={"center"} alignItems={"center"}>
			{trainingHistories}
		</Stack>
	);
};

export default TrainingHistoryInfo;
