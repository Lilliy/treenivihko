import React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Container from '@mui/material/Container';

// Empty bar at the top of the page. For users who are not logged in.
const EmptyAppBar = () => {

	return (
		<AppBar position="static">
			<Container maxWidth="xl">
				<Toolbar disableGutters>
					{/* desktop näkymä */}

					{/* mobiilinäkymä */}
					<Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>


					</Box>

					{/* Mobiilinäkymä */}


					{/* desktop näkymä */}
					<Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>

					</Box>

					{/* koko ajan näkyvissä */}
					<Box sx={{ flexGrow: 0 }}>


					</Box>
				</Toolbar>
			</Container>
		</AppBar>
	);
};
export default EmptyAppBar;