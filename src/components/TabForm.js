import * as React from 'react';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import LoginForm from '../components/LoginForm';
import RegisterForm from '../components/RegisterForm';

// MUI Tab element which is used here to show either the LoginForm or RegisterForm. The useState takes the value of the first tab and is changed by the user when the user clicks on the other tab. This changes the state and re-renders the site automaticlly. This way both the forms can be on the same page and the user doesn't have to navigate to another page to register.
export default function TabForm() {
	const [value, setValue] = React.useState('1');

	const handleChange = (event, newValue) => {
		setValue(newValue);
	};

	return (
		<Box sx={{ maxWidth: { xs: '100%', sm: '100%' }, typography: 'body1' }}>
			<TabContext value={value}>
				<Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
					<TabList onChange={handleChange} aria-label="lab API tabs example">
						<Tab label="Kirjaudu sisään" value="1" />
						<Tab label="Luo tunnus" value="2" />

					</TabList>
				</Box>
				<TabPanel value="1" sx={{ p: 0 }}>{<LoginForm />}</TabPanel>
				<TabPanel value="2" sx={{ p: 0 }}>{<RegisterForm />}</TabPanel>

			</TabContext>
		</Box>
	);
};
