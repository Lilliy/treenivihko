import React, { useEffect, useState, useContext } from 'react';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { MobileDatePicker } from '@mui/x-date-pickers/MobileDatePicker';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { TextField } from '@mui/material';
import CharLimit from './CharLimit';
import AuthContext from '../context/UserContext';
import Button from '@mui/material/Button';
import beginningAddress from './Components';
import { useNavigate } from 'react-router-dom';

// AXIOS Returns all sports from backend. Request GET. Returns an object that contains requested "data" on success and if it fails the "data" value is empty and status "error".
// Required fields: source ("breeds" / "sports")
// Optional fields: -
const getSportData = async () => {
	return await axios.get(`${beginningAddress}get_data.php?source=sports`);
};
// Empty states where to either load old data for editing or to set new data for new object.
// TODO: "Nämä voisi yhdistää yhdeksi tilaksi, mikä on objekti"
// const [dogInfo, setDogInfo] = useState({
// 	dog_id,
// 	sport_id,
// 	date,
// 	ymsyms ...
// });
const TrainingForm = ({ edit }) => {

	const navigate = useNavigate();

	const { user } = useContext(AuthContext);

	const currentDate = new Date();
	const [dog_id, setDog_id] = useState('');
	const [sport_id, setSport_id] = useState('');
	const [location, setLocation] = useState('');
	const [date, setDate] = useState(currentDate);
	const [text_how_went, setText_how_went] = useState('');
	const [text_what_done, setText_what_done] = useState('');
	const [text_what_next, setText_what_next] = useState('');

	// const clearFields = () => {
	// 	setDog_id('');
	// 	setSport_id('');
	// 	setLocation('');
	// 	setDate('');
	// 	setText_how_went('');
	// 	setText_what_done('');
	// 	setText_what_next('');
	// };

	// gets the id from address
	const { id } = useParams();

	// state which is initialized as an empty array and filled with sport data when rendered for the first time.
	const [sports, setSports] = useState([]);

	// useEffect takes an array as an dependency list and  when the variables in the array change the function is called. If the array is empty it is run at every re-rendering. So for this useEffect it will call the getSportData at every re-rendering and sets the sports in to the wanted state.
	useEffect(() => {
		getSportData()
			.then((response) => {
				setSports(response.data.data);
			})
			.catch((error) => {
				console.log(error, "sport datan haun error");
			})
	}, []);

	// state which is initialized as an empty array and filled with users dogs data when rendered for the first time
	const [dogs, setDogs] = useState([]);

	// useEffect has an array (id(dogs) & user) as a dependency list and  when the variables in the array change the function is called. So for this, when the user or id changes it will run the getAllDogsData and set the data gotten in the setDog state. If the array changes (the user changes the dogs id or removes it) it re-renders the form with the proper data. If the getAllDogsData errors, it will alert the user with the error message from backend.

	// AXIOS Returns all of user's dogs or a single dog, if dog ID is specified. Returns an object that contains requested "data" on success and if it fails the "data" value is empty and status "error". Requires authentication
	// Required fields: -
	// Optional fields: id (dog id)
	useEffect(() => {
		const getAllDogsData = async () => {
			const token = user.token
			if (token) {
				const headers = {
					authorization: `Bearer ${token}`,
				};
				const response = await axios
					.get(`${beginningAddress}get_user_dogs.php`, { headers })
					.catch((event) => {
						alert(event.response.data.error_msg)
					})

				return response;
			}

			return null;

		};
		getAllDogsData()
			.then((response) => {
				setDogs(response.data.data);
			})
			.catch((error) => {
				console.log(error, "koiran datan haun error");
			})
	}, [id, user]);

	// When "edit" changes it checks if edit=true and will get the training data and then set the values gotten as a response.
	// note to self:  useEffect takes an array (edit & id) and  when the variables in the array change the function is called. So for this, when the edit or id changes it will check if edit=true and get the data wanted and set it to the proper states. If the array changes (the user changes the training id or removes it) it re-renders the form and gets the new data or empties the fields if there is no id given
	// AXIOS Gets users single training data from backend. Returns an object that contains requested "data" on success and if it fails the "data" value is empty and status "error". Requires authentication
	// Required fields: id (training id)
	// Optional fields: -

	// TODO kun backend muutettu eikä vaadi enää user id, vaan se välittyy tokenin ja useContext mukana, niin osoite pitää muutta. Required fieldseissä yllä ei ole mainittu nyt User_id, koska muutos on tulossa seuraavassa backend puskussa.
	useEffect(() => {
		const getDogsTrainingData = async () => {
			const token = user.token
			if (token) {
				const headers = {
					authorization: `Bearer ${token}`,
				};
				const response = await axios
					.get(`${beginningAddress}get_user_training_reports.php?id=${id}`, { headers })
					.catch((event) => {
						alert(event.response.data.error_msg)
					})

				return response;
			}
			return null;
		};
		if (edit) {
			getDogsTrainingData()
				.then((response) => {
					setDog_id(response.data.data.dog_id)
					setSport_id(response.data.data.sport_id)
					setLocation(response.data.data.location)
					setDate(response.data.data.date)
					setText_how_went(response.data.data.text_how_went)
					setText_what_done(response.data.data.text_what_done)
					setText_what_next(response.data.data.text_what_next)
				})
				.catch((error) => {
					console.log(error, "treenin datan haun error");
				})
		}

	}, [edit, id, user]);

	// Saving a report. If edit=true AXIOS is patch for modifying an old report, if edit=false AXIOS is put for saving a new training report.
	const addNewTraining = (event) => {
		event.preventDefault()
		const newTrainingObject = {
			dog_id: dog_id,
			sport_id: sport_id,
			location: location,
			date: date,
			text_how_went: text_how_went,
			text_what_done: text_what_done,
			text_what_next: text_what_next
		}
		// AXIOS patch is used when an old training report is modified.
		// Required fields: sport_id, dog_id, date
		// Optional fields: location, text_how_went, text_what_done, text_what_next

		// TODO errorhandling. Backend palauttaa mitä? Alert johon backendiltä tullut error teksti?
		// TODO uudelleenohjaus onnistumisen jälkeen? Errorista pysytään sivulla? Tyhjeneekö kentät, vai voiko ne pysyä tallessa ettei käyttäjän tarvitse kirjoittaa kaikkea uudelleen?
		if (edit) {
			const modifyTrainingReport = async (newTrainingObject) => {
				const token = user.token
				if (token) {
					const headers = {
						authorization: `Bearer ${token}`,
					};
					const response = await axios
						.patch(`${beginningAddress}modify_training_report.php`, newTrainingObject, { headers })
						.catch((event) => {
							alert(event.response.data.error_msg)
						})

					return response;
				}

				return null;
			};
			newTrainingObject.id = id;

			modifyTrainingReport(newTrainingObject)
				.then(response => {
					navigate(`/dog/${dog_id}`)
				})
				.catch((error) => {
					console.log(error, "treenin muokkauksen error");
				});
		}
		// AXIOS put is used when saving a new training report.
		// Required fields: sport_id, dog_id, date, location, text_how_went, text_what_done, text_what_next
		// Optional fields: -
		else {
			const saveNewTrainingReport = async (newTrainingObject) => {
				const token = user.token
				if (token) {
					const headers = {
						authorization: `Bearer ${token}`,
					};
					const response = await axios
						.put(`${beginningAddress}save_training_report.php`, newTrainingObject, { headers })
						.catch((event) => {
							alert(event.response.data.error_msg)
						})

					return response;
				}

				return null;
			};

			saveNewTrainingReport(newTrainingObject)
				.then(response => {
					navigate(`/dog/${dog_id}`)
				})
				.catch((error) => {
					console.log(error, "treenin lisäyksen error");
				});
		}

	}
	return (
		<LocalizationProvider dateAdapter={AdapterDateFns}>

			<Box sx={{
				width: "90%",
				my: 0,
				mx: "auto",
				px: "20px",
				py: "20px"
			}}>
				<Typography
					fontSize="30px"
					component="h2"
					pb="20px">
					Lisää treeni
				</Typography>
				<form onSubmit={addNewTraining}>
					<Grid container spacing={3}>
						<Grid item xs={12} sm={6}>
							<Select
								sx={{
									width: "100%"
								}}
								id="dog_id"
								value={dog_id}
								onChange={(event) => setDog_id(event.target.value)}
								displayEmpty
								inputProps={{ 'aria-label': 'Valitse koira' }}
							>
								<MenuItem value="" defaultValue>
									<em>Valitse koira</em>
								</MenuItem>
								{dogs.map(dog =>
									<MenuItem key={dog.id} value={dog.id}>{dog.name}</MenuItem>)
								}
							</Select>
						</Grid>
						<Grid item xs={12} sm={6}>
							<Select
								sx={{
									width: "100%"
								}}
								id="sport_id"
								value={sport_id}
								onChange={(event) => setSport_id(event.target.value)}
								displayEmpty
								inputProps={{ 'aria-label': 'Valitse laji' }}
							>
								<MenuItem value="" defaultValue>
									<em>Valitse laji</em>
								</MenuItem>
								{sports.map(sport =>
									<MenuItem key={sport.id} value={sport.id}>{sport.name}</MenuItem>)
								}
							</Select>
						</Grid>
						<Grid item xs={12} sm={6}>
							<TextField
								sx={{
									width: "100%"
								}}
								id="outlined-basic"
								label="Missä"
								variant="outlined"
								name="location"
								value={location}
								onChange={(event) => setLocation(event.target.value)} />
						</Grid>
						<Grid item xs={12} sm={6}>
							<MobileDatePicker

								label="Date mobile"
								inputFormat="dd/MM/yyyy"
								value={date}
								onChange={(newValue) =>
									setDate(`${newValue.getFullYear()}-${('0' + (newValue.getMonth() + 1)).slice(-2)}-${('0' + (newValue.getDate())).slice(-2)}`)}
								renderInput={(params) => <TextField sx={{
									width: "100%"
								}} {...params} />}
							/>
						</Grid>
						<Grid item xs={12} sm={12}>
							<CharLimit
								setTextHowWent={setText_how_went}
								textHowWent={text_how_went}
							/>
						</Grid>
						<Grid item xs={12} sm={12}>
							<TextField
								id="outlined-basic"
								label="Miten meni?"
								multiline
								rows={4}
								fullWidth
								variant="outlined"
								name="text_what_done"
								value={text_what_done}
								onChange={(event) => setText_what_done(event.target.value)} />
						</Grid>
						<Grid item xs={12} sm={12}>
							<TextField
								id="outlined-basic"
								label="Mitä ensi kerralla?"
								multiline
								rows={4}
								fullWidth
								variant="outlined"
								name="text_what_next"
								value={text_what_next}
								onChange={(event) => setText_what_next(event.target.value)} />
						</Grid>
						<Grid item xs={12} sm={12}>
							<Button
								type="submit"
								variant="contained"
							>Tallenna</Button>
						</Grid>

					</Grid>
				</form>
			</Box>
		</LocalizationProvider>

	);
};

export default TrainingForm;
