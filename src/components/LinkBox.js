import React, { useState } from 'react';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { Box, Tooltip, Menu, MenuItem, Typography, IconButton } from '@mui/material';
import { Link } from 'react-router-dom';


// Used in DogCard & TrainingCard to open a box over the card for user to edit or delete the card data. Requires props for edit or delete from DogCard or TrainingCard. editObject is used to link to a form component to modify the wanted data. deleteObjectId is used to open the DeleteModal component for deleting wanted data, openModal is also used for this.
const LinkBox = ({ editObject, openModal, deleteObjectId }) => {
	//  State here keeps track of the menu being open or closed.
	const [anchorElUser, setAnchorElUser] = useState(null);
	// When user clicks the settings icon it will open a menu box.
	const handleOpenUserMenu = (event) => {
		setAnchorElUser(event.currentTarget);
	};
	// This is used to close the menu
	const handleCloseUserMenu = () => {
		setAnchorElUser(null);
	};
	// Opens the delete modal.
	const handleDelete = () => {
		handleCloseUserMenu();
		openModal(deleteObjectId);
	};
	return (
		<Box>
			<Tooltip title="Open settings">
				<IconButton
					aria-label="settings"
					onClick={handleOpenUserMenu}>
					<MoreVertIcon />
				</IconButton>
			</Tooltip>
			<Menu
				anchorEl={anchorElUser}
				anchorOrigin={{
					vertical: 'top',
					horizontal: 'right',
				}}
				open={Boolean(anchorElUser)}
				onClose={handleCloseUserMenu}
			>
				<MenuItem onClick={handleCloseUserMenu}
					component={Link}
					to={editObject}>
					<Typography textAlign="center">Muokkaa</Typography>
				</MenuItem>
				<MenuItem onClick={handleDelete}>
					<Typography textAlign="center">Poista</Typography>
				</MenuItem>
			</Menu>
		</Box>
	);
};

export default LinkBox;