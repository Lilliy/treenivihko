import React from 'react';

import { TextField } from '@mui/material';

export default function CharLimit({ setTextHowWent, textHowWent }) {
	const CHARACTER_LIMIT = 250;


	return (
		<>
			<TextField
				label="Lyhyesti"
				multiline
				rows={4}
				fullWidth
				inputProps={{
					maxLength: CHARACTER_LIMIT
				}}
				value={textHowWent}
				helperText={`${textHowWent.length}/${CHARACTER_LIMIT}`}
				onChange={(event) => setTextHowWent(event.target.value)}
				margin="normal"
				variant="outlined"
			/>
		</>
	);
}


