import React, { useContext } from 'react';
import ResponsiveAppBar from '../components/ResponsiveAppBar';
import EmptyAppBar from '../components/EmptyAppBar';
import AuthContext from '../context/UserContext';

// isLoggedIn has the data if user is logged in and it is fetched from useContext.
// Ternary is used to show the different AppBars to logged in and non logged in users. 
const AppBar = () => {
	const { isLoggedIn } = useContext(AuthContext);
	return (
		<div>
			{isLoggedIn ? <ResponsiveAppBar /> :
				<EmptyAppBar />
			}
		</div>
	);
};

export default AppBar;
