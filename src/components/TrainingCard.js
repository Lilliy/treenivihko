import React, { useState, useContext } from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import LinkBox from '../components/LinkBox';
import DeleteModal from '../components/DeleteModal';
import axios from 'axios';
import AuthContext from '../context/UserContext';
import beginningAddress from './Components';

// import Chip from '@mui/material/Chip';
// import { Box } from '@mui/material';

// MUI component. Button on a card that expands the card
const ExpandMore = styled((props) => {
	const { expand, ...other } = props;
	return <IconButton {...other} />;
})
	// styling
	(({ theme, expand }) => ({
		transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
		marginLeft: 'auto',
		transition: theme.transitions.create('transform', {
			duration: theme.transitions.duration.shortest,
		}),
	}));

// props are passed down from DogPage to TrainingHistoryInfo to here.
// training: data gotten from backend and set into useState.
// refresh: boolean,  used to reload the page after a training card is deleted.
export default function TrainingCard({ training, refresh }) {
	const [expanded, setExpanded] = useState(false);
	const { user } = useContext(AuthContext);
	// This expands or minimizes the card and sets the boolean value to the opposite of the current value.
	const handleExpandClick = () => {
		setExpanded(!expanded);
	};
	// state to control if the LinkBox is opened or not.
	const [openModal, setOpenModal] = useState(false);
	const handleOpen = () => setOpenModal(true);
	const handleClose = () => setOpenModal(false);

	// For deleting a training the required data is set into an object.
	// TODO: Backend poistaa vaatimuksen User Id:stä! 
	const deleteObject = () => {
		// AXIOS Deletes users single dog's training data from backend. Returns "status=ok" on success and if it fails "status=error". Requires authentication
		// Deletes all data related to the spesified dog's training.
		// Required fields: id (training.id), type ("training_report")
		// Optional fields: 
		const deleteTraining = async () => {
			const token = user.token
			if (token) {
				const headers = {
					authorization: `Bearer ${token}`,
				};
				const response = await axios
					.delete(`${beginningAddress}delete_data.php?id=${training.id}&type=training_report`, { headers })
					.catch((event) => {
						alert(event.response.data.error_msg)
					})

				return response;
			}
			return null;
		};

		deleteTraining()
			.then(response => {
			})
			.catch((error) => {
				console.log(error, "koiran poiston error");
			});

		handleClose();
		refresh(true);
	};

	return (
		<>
			<Card sx={{
				width: { xs: "90%", sm: "500px", md: "600px" },
				position: "relative",
				pb: "20px"
			}}>
				<CardHeader
					avatar={
						<Avatar style={{ backgroundColor: `${training.sport_color}` }} aria-label="recipe">
							{training.sport_abb}
						</Avatar>
					}
					action={
						<LinkBox
							openModal={handleOpen}
							editObject={`/trainingedit/${training.id}`}
							deleteObjectId={training.id} />
					}
					title={training.sport_name}
					subheader={`${training.date} ${training.location}`}
				/>

				<CardContent>
					<Typography variant="body2" color="text.secondary" sx={{ paddingRight: "40px" }}>
						{training.text_how_went}
					</Typography>
				</CardContent>
				<CardActions disableSpacing
					sx={{
						position: "absolute",
						right: "0",
						bottom: "0"
					}}>
					<ExpandMore
						expand={expanded}
						onClick={handleExpandClick}
						aria-expanded={expanded}
						aria-label="show more">
						<ExpandMoreIcon />
					</ExpandMore>
				</CardActions>
				<Collapse in={expanded} timeout="auto" unmountOnExit>
					<CardContent>
						<Typography variant="h6">Miten meni</Typography>
						<Typography paragraph >{training.text_what_done}</Typography>
						<Typography variant="h6">Ideoita seuraavaan</Typography>
						<Typography paragraph>{training.text_what_next}</Typography>
						{/* <Box sx={{ maxWidth: "calc(100% - 40px)" }}>
						{training.exercises.map(exercise =>
							<Chip sx={{ m: "2px" }} label={exercise} />)}
					</Box> */}
					</CardContent>
				</Collapse>
			</Card>
			<DeleteModal
				open={openModal}
				setClose={handleClose}
				target={"treenin"}
				deleteObject={deleteObject} />
		</>
	);
}
