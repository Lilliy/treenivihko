import React, { useState, useContext } from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import CardMedia from '@mui/material/CardMedia';
import CardHeader from '@mui/material/CardHeader';
import Typography from '@mui/material/Typography';
import { Stack, Chip } from '@mui/material';
import { Link } from 'react-router-dom';
import LinkBox from '../components/LinkBox';
import DeleteModal from '../components/DeleteModal';
import axios from 'axios';
import AuthContext from '../context/UserContext';
import beginningAddress from './Components';

// Dog data is passed here from AllDogs
const DogCard = (dog) => {

	const { user } = useContext(AuthContext);

	// State for modal to open or close by the user.
	const [openModal, setOpenModal] = useState(false);
	const handleOpen = () => setOpenModal(true);
	const handleClose = () => setOpenModal(false);

	const deleteObject = () => {
		// AXIOS Deletes users single dog's data from backend. Returns "status=ok" on success and if it fails "status=error". Requires authentication
		// Deletes all data related to the spesified dog. In other words, when a dog is deleted, it deletes all training reports related to the dog.
		// Required fields: id (dog.id), type ("dog")
		// Optional fields: 
		const deleteDog = async () => {
			const token = user.token
			if (token) {
				const headers = {
					authorization: `Bearer ${token}`,
				};
				const response = await axios
					.delete(`${beginningAddress}delete_data.php?id=${dog.id}&type=dog`, { headers })
					.catch((event) => {
						alert(event.response.data.error_msg)
					})

				return response;
			}
			return null;
		};

		deleteDog()
			.then(response => {
				handleClose();
				dog.refresh(true);
			})
			.catch((error) => {
				console.log(error, "koiran poiston error");
			});


	};

	return (
		<>
			<Card sx={{
				width: { xs: 250, sm: 400, md: 500, },
				maxHeight: { xs: 300, sm: 400, md: 500 }
			}}>
				<CardHeader
					action={
						<LinkBox
							openModal={handleOpen}
							editObject={`/dogform/${dog.id}`}
							deleteObjectId={dog.id} />
					}
					title={dog.name}
				/>
				<Stack direction="row">
					<CardMedia
						component="img"
						height="100%"
						image={dog.image_filename}
						alt="Koiran kuva"
						sx={{ width: "50%" }}
					/>
					<CardContent sx={{ width: "50%" }}>
						<Typography sx={{ display: "inline" }}>Lajit:</Typography>
						{dog.sports?.map(sport =>
							<Chip sx={{ m: "2px" }} key={Math.floor(Math.random() * 10 + Date.now())} label={sport} />)}
						<div>
							<Typography sx={{ display: "inline" }}>Viimeksi tehty: </Typography><Chip label="Toko"></Chip>
						</div>
					</CardContent>
				</Stack>
				<CardActions>
					<Button size="small" component={Link} to={`/dog/${dog.id}`}>
						Kaikki tiedot
					</Button>
				</CardActions>
			</Card>
			<DeleteModal
				open={openModal}
				setClose={handleClose}
				target={"koiran"}
				deleteObject={deleteObject} />
		</>
	);
}

export default DogCard;