const dogs = [
	{
		id: 1,
		name: "Vixi",
		officialName: "Ketunpolun Jeanne d'Arc",
		breed: "Belgianpaimenkoira tervueren",
		dob: "19.04.2004",
		sports: ["Toko", "Agility", "Vesipelastus", "Rally-toko"],
		picture: "https://mudi-info.com/belgit/pic/vixi_main3.jpg"
	},
	{
		id: 2,
		name: "Dexi",
		officialName: "Lyncis",
		breed: "Belgianpaimenkoira tervueren",
		dob: "13.09.2020",
		sports: ["Toko", "Agility", "Peltojälki"],
		picture: "https://mudi-info.com/belgit/pic/dexi-main.jpg"
	},
	{
		id: 3,
		name: "Navi",
		officialName: "Dysi",
		breed: "Belginapimenkoira groenendael",
		dob: "02.04.2021",
		sports: ["Agility"],
		picture: "https://mudi-info.com/belgit/pic/navi-main.jpg"
	}
]

export default dogs;