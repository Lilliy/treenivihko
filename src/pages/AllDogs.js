import { Stack } from '@mui/material';
import { useEffect, useState, useContext } from 'react';
import DogCard from '../components/DogCard';
import axios from 'axios';
import AuthContext from '../context/UserContext';
import beginningAddress from '../components/Components';

// Empty state where to data from backend is set.
const AllDogs = ({ id }) => {

	const { user } = useContext(AuthContext);
	const [dogs, setDogs] = useState([]);
	const [refresh, setRefresh] = useState(false);

	// useEffect re-renders the component when the variables in the array (id, refresh, user) changes. Used here for when user deletes a dog, the state of "refresh" will change and re-render the page.

	// AXIOS Returns all of user's dogs or a single dog, if dog ID is specified. Returns an object that contains requested "data" on success and if it fails the "data" value is empty and status "error". Requires authentication
	// Required fields: -
	// Optional fields: id (dog id)
	useEffect(() => {
		const getAllDogsData = async () => {
			const token = user.token
			if (token) {
				const headers = {
					authorization: `Bearer ${token}`,
				};
				const response = await axios
					.get(`${beginningAddress}get_user_dogs.php`, { headers })
					.catch((event) => {
						alert(event.response.data.error_msg)
					})

				return response;
			}
			return null;
		};
		getAllDogsData()
			.then((response) => {
				setDogs(response.data.data);
				setRefresh(false);
			})
			.catch((error) => {
				console.log(error, "koiran datan haun error");
			})
	}, [id, refresh, user]);

	return (
		<div>
			<Stack spacing={"30px"} sx={{ alignItems: "center", justifyContent: "center", paddingTop: "30px", }}>
				{dogs.map(dog =>
					<DogCard
						key={dog.id}
						name={dog.name}
						sports={dog.sports}
						image_filename={dog.image_filename}
						id={dog.id}
						refresh={setRefresh} />)
				}
			</Stack>

		</div>
	);
};

export default AllDogs;
