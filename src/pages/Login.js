import React from 'react';
import LoginForm from '../components/LoginForm';

// Page component where it's job is to return the form component

const Login = () => {
	return (
		<LoginForm />
	);
};

export default Login;
