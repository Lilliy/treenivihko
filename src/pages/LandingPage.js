import React, { useContext } from 'react';
import LandingPageInfo from '../components/LandingPageInfo';
import TabForm from '../components/TabForm';
import AuthContext from '../context/UserContext';
import { Stack, Typography } from '@mui/material';

const LandingPage = () => {
	const { isLoggedIn } = useContext(AuthContext);
	// checks from Context if isLoggedIn is true or not and depending on that will show different things. If isLoggedIn=true will only show the LandingPageInfo which includes text that is relevant to users who are in and who are not. If isLoggedIn=false it will show the TabForm besides the LandingPageInfo. The TabForm has the login and register components in it, so it is only relevant with users who are not logged in.
	return (
		<Stack direction={{ xs: 'column', sm: 'row' }}
			px={{ xs: '10px', sm: '20px', md: '50px' }}
		>
			<Stack maxWidth={{ xs: '100%', sm: '50%', md: '60%' }}>
				<Typography
					align="left"
					letterSpacing={{ xs: '5px', sm: '10px', md: '15px' }}
					py={{ xs: '10px', sm: '20px', md: '30px' }}
					pl={"20px"}
					fontSize={{ xs: '30px', sm: '30px', md: '50px' }}
					component="h1">
					Treenivihko</Typography>

				<LandingPageInfo />
			</Stack>
			{!isLoggedIn && <TabForm />}

		</Stack >
	);
};

export default LandingPage;
