import React from 'react';
import TrainingForm from '../components/TrainingForm';

// From routes the edit is either true or false and it is passed down to the TrainingForm component. It will affect the form, that if edit=true the forms fields will be filled with data gotten from backend, and if edit=false it will be a new training report and fields will be empty. Also the axios call will differ depending on the edit being true/false.

const TrainingContainer = ({ edit }) => {

	return (
		<TrainingForm edit={edit} />
	);
};

export default TrainingContainer;
