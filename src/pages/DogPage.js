import React, { useEffect, useState, useContext } from 'react';
import DogInfo from '../components/DogInfo';
import TrainingHistoryInfo from '../components/TrainingHistoryInfo';
import { Stack } from '@mui/material';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import AuthContext from '../context/UserContext';
import beginningAddress from '../components/Components';

// AXIOS Returns all of user's dogs or a single dog, if dog ID is specified. Returns an object that contains requested "data" on success and if it fails the "data" value is empty and status "error". Requires authentication
// Required fields: -
// Optional fields: id (dog id)


const DogPage = () => {
	const { user } = useContext(AuthContext);
	// useParams gets the dog's id from the URL. It is a key/value pair defined in Routes.
	const { id } = useParams();

	// dog, setDog : Empty state for the data from backend.
	// refresh, setRefresh : boolean state used to show changes immediately when a TrainingCard is deleted.
	const [dog, setDog] = useState({});
	const [refresh, setRefresh] = useState(false);

	// useEffect re-renders the component when the variables in the array (id, refresh, user) changes. Used here for when user deletes a dog, the state of "refresh" will change and re-render the page.

	// AXIOS Returns all of user's dogs or a single dog, if dog ID is specified. Returns an object that contains requested "data" on success and if it fails the "data" value is empty and status "error". Requires authentication
	// Required fields: -
	// Optional fields: id (dog id)
	useEffect(() => {
		const getDogData = async (id) => {
			const token = user.token
			if (token) {
				const headers = {
					authorization: `Bearer ${token}`,
				};
				const response = await axios
					.get(`${beginningAddress}get_user_dog_training_reports.php?dog_id=${id}`, { headers })
					.catch((event) => {
						alert(event.response.data.error_msg)
					})

				return response;
			}
			return null;
		};
		getDogData(id)
			.then((response) => {
				setDog(response.data.data);
				setRefresh(false);
			})
			.catch((error) => {
				console.log(error, "with dog data");
			})
	}, [id, refresh, user]);

	return (
		<Stack spacing={"20px"} sx={{ alignItems: "center", justifyContent: "center", paddingTop: "30px" }}>
			<DogInfo dog={dog} />
			<TrainingHistoryInfo trainings={dog.trainings} refresh={setRefresh} />

		</Stack>
	);
}

export default DogPage;
