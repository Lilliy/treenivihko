import React from 'react';
import RegisterForm from '../components/RegisterForm';

// Page component where it's job is to return the register form component. Right now in the project this page is not in use, but it can be used later as a linked single page for new users to register.

const RegisterPage = () => {
	return (
		<RegisterForm />
	);
};

export default RegisterPage;
