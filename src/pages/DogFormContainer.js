import React from 'react';
import DogForm from '../components/DogForm';

// From routes the edit is either true or false and it is passed down to the DogForm component.

const DogFormContainer = ({ edit }) => {
	return (
		<DogForm edit={edit} />
	);
};

export default DogFormContainer;
