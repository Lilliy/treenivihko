import React, { useContext } from 'react';
import { Routes, Route } from 'react-router-dom';
import LandingPage from '../pages/LandingPage';
import AllDogs from '../pages/AllDogs';
import DogPage from '../pages/DogPage';
import DogFormContainer from '../pages/DogFormContainer';
import Login from '../pages/Login';
import RegisterPage from '../pages/RegisterPage';
import TrainingContainer from '../pages/TrainingContainer';
import TrainingPlan from '../pages/TrainingPlan';
import AuthContext from '../context/UserContext';


const Router = () => {
	const { isLoggedIn } = useContext(AuthContext);
	return (
		<Routes>
			<Route
				exact path="/"
				element={
					<LandingPage />
				}
			/>
			<Route
				exact path="/login"
				element={
					<Login />
				}
			/>
			<Route
				exact path="/registerpage"
				element={
					<RegisterPage />
				}
			/>

			<Route
				exact path="/alldogs"
				element={isLoggedIn ?
					<AllDogs /> :
					<LandingPage />
				}
			/>
			<Route
				exact path="/dog"
				element={isLoggedIn ?
					<DogPage /> :
					<LandingPage />
				}
			/>
			<Route
				exact path="/dog/:id"
				element={isLoggedIn ?
					<DogPage /> :
					<LandingPage />
				}
			/>
			<Route
				exact path="/dogform"
				element={isLoggedIn ?
					<DogFormContainer edit={false} /> :
					<LandingPage />
				}
			/>
			<Route
				exact path="/dogform/:id"
				element={isLoggedIn ?
					<DogFormContainer edit={true} /> :
					<LandingPage />
				}
			/>
			<Route
				exact path="/trainingcontainer"
				element={isLoggedIn ?
					<TrainingContainer edit={false} /> :
					<LandingPage />
				}
			/>
			<Route
				exact path="/trainingedit/:id"
				element={isLoggedIn ?
					<TrainingContainer edit={true} /> :
					<LandingPage />
				}
			/>
			<Route
				exact path="/trainingplan"
				element={isLoggedIn ?
					<TrainingPlan /> :
					<LandingPage />
				}
			/>
			<Route
				path="*"
				element={
					<>Page not found</>
				} /></Routes>
	);
};

export default Router;
