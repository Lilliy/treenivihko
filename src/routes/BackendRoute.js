import React, { useContext } from 'react';
import AuthContext from '../context/UserContext';
import axios from 'axios';


// Page component where it's job is to return the form component

const BackendRoute = ({ callAddress }) => {
	const { user } = useContext(AuthContext);


	const backendCallFunction = async () => {
		const token = user.token
		if (token) {
			const headers = {
				authorization: `Bearer ${token}`,
			};
			const response = await axios
				.delete(`http://localhost/treenivihko/API/${callAddress}`, { headers })
				.catch((event) => {
					alert(event.response.data.error_msg)
				})

			return response;
		}
		return null;
	};
	return (
		<></>
	);
};

export default BackendRoute;
