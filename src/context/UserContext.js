import React, { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from 'react-router-dom';
import beginningAddress from '../components/Components';

// Defenitions, for React to read them properly? 
const AuthContext = React.createContext({
	isLoggedIn: false,
	onLogOut: () => { },
	onLogIn: () => { },
	user: undefined
});

export const AuthContextProvider = ({ children }) => {
	const [user, setUser] = useState(null)
	const [loggedIn, setLoggedIn] = useState(false);
	const navigate = useNavigate();

	// useEffect takes an array as an dependency list and  when the variables in the array change the function is called. If the array is empty it is run at the first rendering. Here it gets the token from localStorage and then checks that if it is not undefined nor null, it will set the state of LoggedIn to true, get username from localStorage and set the state of User with object of token and username. 
	useEffect(() => {
		const token = localStorage.getItem("token");
		if (token !== undefined && token !== null) {
			setLoggedIn(true);
			const username = localStorage.getItem("username");
			console.log(username);
			setUser({
				token,
				username
			});
		}
	}, []);

	// Function that takes the parameters username and password. const credentials is written in shorthand. The full way to type it, is
	// username: username,
	// 	password: password
	const handleLogIn = (username, password) => {
		const credentials = {
			username,
			password
		};
		// AXIOS sends a request with credentials. If the response is valid, loginData takes the values given, the User state is set with the loginData, LoggedIn state is set to true and username and token is saved to localStorage. 
		// if the axios response is not valid it will alert the user with the proper message.
		axios
			.post(`${beginningAddress}authorize.php`, credentials)
			.then(response => {
				console.log('kirjautui! ', response.data.data)
				const loginData = {
					username: username,
					token: response.data.data
				}
				setUser(loginData);
				setLoggedIn(true);
				console.log(loginData);
				window.localStorage.setItem(
					'username', loginData.username
				);
				window.localStorage.setItem(
					'token', loginData.token
				);
				navigate("/");
			})
			.catch((event) => {
				console.log(event, "Ei kirjautunu!");
				alert(event.response.data.error_msg);
			});
	}
	// This function logs out the user. It sets the state of loggedIn to false, empties the state of User and removes items (username & token) from localStorage
	const handleLogOut = () => {
		setLoggedIn(false);
		setUser("");
		localStorage.removeItem("username");
		localStorage.removeItem("token");
	};

	// AuthContext.Provider is a wrapper and the direct children can us the given values.
	return (
		<AuthContext.Provider
			value={{
				isLoggedIn: loggedIn,
				onLogIn: handleLogIn,
				onLogOut: handleLogOut,
				user: user
			}}
		>
			{children}
		</AuthContext.Provider>
	);
};

export default AuthContext;