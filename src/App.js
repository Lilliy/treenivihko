import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Router from './routes/Routes';
import AppBar from './components/AppBar';
import { AuthContextProvider } from './context/UserContext';
import './style.css';



function App() {

	return (
		<div className="container">
			<BrowserRouter
			// basename={"/treenivihko"}
			>
				<AuthContextProvider>
					<AppBar />

					<Router />
				</AuthContextProvider>
			</BrowserRouter>
			<footer>
				<p>Made by Lilli Luomakoski</p>
			</footer>
		</div>
	);
}

export default App;
